# Markdown Previewer

A simple [Markdown](https://en.wikipedia.org/wiki/Markdown) Previewer made with
[React](https://reactjs.org/) for learning purposes.

[Check it out](https://dreamtigers.github.io/markdown-previewer/)

## Table of Contents

- [Explanation](#explanation)
- [Cloning](#Cloning)
- [Deployment](#deployment)
- [Resources](#resources)

## Explanation

React uses something called **Components** that are somewhat like classes
with special methods. This project consists of 3 Components:

* `App.js` is the main component, it's the parent of both `Input` and `Output`.

* `Input.js` is where we introduce our Markdown text.

* `Output.js` recieves the Markdown text, transforms it and displays the HTML
result.

```js
<App>
  ...
  <Input ... />
  ...
  <Output ... />
</App>
```

> How do we send the text from Input to Output?

React's components can _only_ send data (called _properties_ or **props**)
from a **Parent** to a **Child** and viceversa.

> But Input and Output are siblings! How do we send data from one to another?

We have to send the data from Input to App (Child to Parent), and from App to
Output (Parent to Child).

> How do we do that?

In our App component, we have a `constructor`, it's the method that will
generate our component. In our constructor we define a `state`, which belongs
to the Class/Component, each component handles it's own state. That state
contains all of the component's props. In this case, a `text`. This `text` is
the data that we'll send from Input to App, and from App to Output... in other
words, `text` it's our Markdown text.

We'll pass two properties to the Input component, the `text` that we've talked
about, and a function `updateApp`:

```js
<Input text={this.state.text} updateApp={this.updateApp} />
```

This is how we pass a property from a Parent to it's Child. Pretty straight
forward.

The `updateApp` method belongs to **App**, and it changes the value of App's
`text` making use of the React method `setState`. We send this method to Input,
this way, Input, through  the `updateApp` method (which looks like
`this.props.updateApp`) can change the state of App.

We send data from a Child to a Parent through a callback, like `updateApp`.

Now we define an `update` method (that belongs to Input), and will call the
`updateApp` method that Input recieved from App:

```js
update(event) {
  var newValue = event.target.value;

  this.props.updateApp(newValue);
}
```

As a side note: see how the `updateApp` method belongs to Input's props?
That's because when we passed the method in App, it became part of Input's
properties, but not it's state.

Now we bind the `update` method to the `<textarea>` tag like this:

```js
<textarea onChange={this.update} >
```

`onChange` is another of React's special methods, what it does is basically
"everytime this tag changes, execute whatever is binded here" (in this case
`this.update`).

We start writing inside our `<textarea>` and each keystroke triggers
`this.update`, which recieves the event, gets the target, and gets that
target's value. In this case, whatever happens to be inside the tag:

```js
  var newValue = event.target.value;
```

So `this.update` changes App's `state.text` everytime `<textarea>` changes,
then App sends it's `state.text` to Output:

```js
<Output text={this.state.text} />
```

And the `marked` package does the rest.

## Cloning

```sh
$ git clone https://github.com/dreamtigers/markdown-previewer.git
$ cd markdown-previewer
$ yarn install
$ yarn start
```

## Deployment

This project uses the `gh-pages` package to deploy, so all you have to do is
specify your homepage in package.json:

```js
  "homepage": "https://myusername.github.io/my-app",
```

And run the script:

```sh
$ yarn run deploy
```

## Resources

https://stackoverflow.com/questions/34686523/using-marked-in-react

https://medium.com/@arpith/handling-markdown-in-react-24b275cddf39

https://reactjs.org/docs/dom-elements.html#dangerouslysetinnerhtml
