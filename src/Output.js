import React, { Component } from 'react';
import marked from 'marked';

class Output extends Component {

  constructor(props) {
    super(props);

    this.createMarkup = this.createMarkup.bind(this);
  }

  createMarkup(markdown) {
    var rawMarkup = marked(markdown, {sanitize: true});
    return { __html: rawMarkup };
  }

  render() {
    return (
      <div className="Output" dangerouslySetInnerHTML={this.createMarkup(this.props.text)} />
    );
  }

}

export default Output;
