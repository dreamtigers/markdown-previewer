import React, { Component } from 'react';
import Input from './Input';
import Output from './Output';
import markdown from './markdown.md';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      text: ''
    }

    this.updateApp = this.updateApp.bind(this);
  }

  updateApp(newValue) {
    this.setState(
      {text: newValue}
    )
  }

  componentDidMount() {
    fetch(markdown)
    .then(response => response.text())
    .then(text => this.setState( {text: text} ));
  }

  render() {
    return (
      <div className="container">
	<div className="row">
	  <div className="col-md-6">
	    <Input text={this.state.text} updateApp={this.updateApp} />
	  </div>
	  <div className="col-md-6">
	    <Output text={this.state.text} />
	  </div>
	</div>
      </div>
    );
  }

}

export default App;
