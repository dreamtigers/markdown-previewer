A Markdown Previewer
=======

Made in React
-----------

### With lots of headers

Text attributes *italic*, **bold**,
`monospace`, ~~strikethrough~~ .

Nobody expects
`The Spanish Inquisition`

Amongst our weaponry are such elements as:

  * Fear
  * Surprise
  * Ruthless Efficiency
  * An almost fanatical devotion to the Pope
  * Nice red uniforms

Clone and run the project:

```
$ git clone https://github.com/dreamtigers/markdown-previewer.git
$ cd markdown-previewer
$ yarn start
```

 *[A nice link](https://github.com/dreamtigers/markdown-previewer)*
