import React, { Component } from 'react';

class Input extends Component {

  constructor(props) {
    super(props);

    this.update = this.update.bind(this);
  }

  update(event) {
    var newValue = event.target.value;

    this.props.updateApp(newValue);
  }

  render() {
    return (
      <textarea value={this.props.text} onChange={this.update}>
      </textarea>
    );
  }
}

export default Input;
